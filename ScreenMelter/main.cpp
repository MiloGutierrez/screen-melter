#include <Windows.h>

int ScreenWidth, ScreenHeight;
int Interval = 10;

LRESULT CALLBACK Melter(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	switch (Msg)
	{
	case WM_CREATE:
	{
		HDC Desktop = GetDC(HWND_DESKTOP);
		HDC Window = GetDC(hWnd);

		BitBlt(Window, 0, 0, ScreenWidth, ScreenHeight, Desktop, 0, 0, SRCCOPY);
		ReleaseDC(hWnd, Window);
		ReleaseDC(HWND_DESKTOP, Desktop);

		SetTimer(hWnd, 0, Interval, 0);
		ShowWindow(hWnd, SW_SHOW);
		break;
	}
	case WM_PAINT:
	{
		ValidateRect(hWnd, 0);
		break;
	}
	case WM_TIMER:
	{
		HDC Window = GetDC(hWnd);
		int X = (rand() % ScreenWidth) - (150 / 2),
			Y = (rand() % 15),
			Width = (rand() % 150);
		BitBlt(Window, X, Y, Width, ScreenHeight, Window, X, 0, SRCCOPY);
		ReleaseDC(hWnd, Window);
		break;
	}
	case WM_DESTROY:
	{
		KillTimer(hWnd, 0);
		PostQuitMessage(0);
		break;
	}
	return 0;
	}
	return DefWindowProc(hWnd, Msg, wParam, lParam);
}

int APIENTRY WinMain(HINSTANCE Inst, HINSTANCE Prev, LPSTR Cmd, int showcmd)
{
	//Sleep(1800000);
	// Get the width & height of current display
	ScreenWidth = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	ScreenHeight = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	WNDCLASS wndClass = { 0, Melter, 0,0, Inst, 0, LoadCursorW(0, IDC_ARROW), 0, 0, L"ScreenMelter" };
	
	if (RegisterClass(&wndClass))
	{
		// Create the "melter" overlapping window.
		HWND hWnd = CreateWindowExA(WS_EX_TOPMOST, "ScreenMelter", 0, WS_POPUP,
			0, 0, ScreenWidth, ScreenHeight, HWND_DESKTOP, 0, Inst, 0);
		if (hWnd)
		{
			// seed for randomization
			srand(GetTickCount());
			MSG Msg = { 0 };
			int exit = 1;
			// Run the melter loop
			while (exit == 1)
			{
				if (PeekMessage(&Msg, 0, 0, 0, PM_REMOVE))
				{
					if (Msg.message == WM_SYSKEYDOWN)
					{
						if (Msg.wParam == VK_F4)
						{
							Msg.wParam = VK_F3;
						}
					}
					TranslateMessage(&Msg);
					DispatchMessage(&Msg);
				}
				if (GetAsyncKeyState(VK_ESCAPE) && GetAsyncKeyState(VK_F1))
				{
					exit == 0;
					Msg.message = WM_QUIT;
					DestroyWindow(hWnd);
					ExitProcess(0);
				}				
			}
		}
	}
	return 0;
}